<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" version="1.0">
<xsl:output method="xml"/>
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width"/>
<title><xsl:value-of select="rss/channel/title"/> (RSS)</title>
<!-- style><![CDATA[html{margin:0;padding:0;}body{color:hsl(180,1%,31%);font-family:Helvetica,Arial,sans-serif;font-size:17px;line-height:1.4;margin:5%;max-width:35rem;padding:0;}input{min-width:20rem;margin-left:.2rem;padding-left:.2rem;padding-right:.2rem;}ol{list-style-type:disc;padding-left:1rem;}]]></style -->
<style><![CDATA[:root{color-scheme:light dark;--primary:light-dark(#0969da,#58a6ff);--light-primary:light-dark(#218bff,#79c0ff);--red:light-dark(#cf222e,#ff7b72);--black:light-dark(#0d1117,#ffffff);--white:light-dark(#e6edf3,#1f2328);--dark-gray:light-dark(#24292f,#484f58);--gray:light-dark(#6e7781,#6e7781);--light-gray:light-dark(#57606a,#b1bac4);--lighter-gray:light-dark(#8c959f,#dddedf);--font-sans-serif:"atkinson hyperlegible",system-ui,-apple-system,segoe ui,roboto,ubuntu,helvetica,cantarell,noto sans,sans-serif;*{box-sizing:border-box;margin:0;padding:0;text-rendering:geometricPrecision;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;-webkit-tap-highlight-color:transparent;font-family:var(--font-sans-serif)}html{font-size:calc(16px + ((100vw - 600px) / 250));padding:0;text-decoration-skip-ink:"auto";line-height:1.953rem;margin:auto;min-height:100%;overflow-x:hidden;max-width:1140px}body{padding:0;margin:calc((100vh / 25) * 1.563) calc((100vw / 25) * 1.563);background-color:var(--white);color:var(--black);caret-color:var(--black);word-wrap:break-word}h1{margin-bottom:1rem;margin-top:1em;font-weight:bold;font-size:3.052rem;line-height:1}h2{margin-bottom:1rem;margin-top:1em;font-weight:bold;font-size:2.441rem;letter-spacing:-0.12rem;line-height:1.2;};p{line-height:1.5;margin-bottom:1.263rem}input{min-width:20rem;margin-left:.2rem;padding:.4rem}ul{margin:0;padding:0 1px;list-style:disc outside;font-variant-numeric:tabular-nums;padding-left:1rem;margin-bottom:1rem}li{list-style-position:inside;padding-top:.2rem;padding-bottom:.2rem}a{color:var(--primary);text-decoration:none}a:hover{text-decoration:underline}]]></style>
<link rel="icon" href="favicon.ico" />
<link rel="icon" href="icon.svg" />
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="manifest" href="site.webmanifest" />
</head>
<body>
<h1><xsl:value-of select="rss/channel/title"/> (RSS)</h1>
<p>This is a styled preview of my <abbr title="Really Simple Syndication">RSS</abbr> feed. You can subscribe to it by copying the URL into your newsreader. New to feeds? Just visit <a href="https://aboutfeeds.com/" title="About Feeds">About Feeds</a> to learn more and get started.</p>
<p>
<label for="address">RSS address:</label>
<input><xsl:attribute name="type">url</xsl:attribute><xsl:attribute name="id">address</xsl:attribute><xsl:attribute name="spellcheck">false</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="rss/channel/atom:link[@rel='self']/@href"/></xsl:attribute></input>
</p>
<h2>Latest Posts:</h2>
<ul>
<xsl:for-each select="rss/channel/item">
<li><a><xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute><xsl:value-of select="title"/></a></li>
</xsl:for-each>
</ul>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
